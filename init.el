;;; init.el --- Spacemacs Initialization File
;;
;; Copyright (c) 2012-2016 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;; Without this comment emacs25 adds (package-initialize) here
;; (package-initialize)


;;;;;;;;;;;
;; EMACS ;;
;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CUSTOM SET VARIABLES ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(menu-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(make-backup-files nil)
 '(auto-save-default nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum ((t (:inherit (shadow default) :foreground "white")))))


;;;;;;;;;;;;;;;;;;
;; REPOSITORIES ;;
;;;;;;;;;;;;;;;;;;
(require 'package)
(setq package-archives '(
    ("ELPA" . "http://tromey.com/elpa/")
    ("gnu"  . "http://elpa.gnu.org/packages/")
    ("SC"   . "http://joseito.republika.pl/sunrise-commander/")))
(add-to-list 'package-archives '(
    "marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '(
    "melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '(
    "melpa" . "http://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))


;;;;;;;;;;
;; JEDI ;;
;;;;;;;;;;
(add-hook 'python-mode-hook 'jedi:setup)


;;;;;;;;;;;;;
;; TWITTER ;;
;;;;;;;;;;;;;
(add-to-list 'load-path "~/.emacs.d/elpa/twittering-mode")
(setq twittering-use-master-password t)
(require 'twittering-mode)


;;;;;;;;;;
;; HELM ;;
;;;;;;;;;;
(add-to-list 'load-path "~/.emacs.d/helm/helm")
(require 'helm-config)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-q") 'helm-mini)


;;;;;;;;;;;;;
;; SUNRISE ;;
;;;;;;;;;;;;;
(global-set-key (kbd "M-s M-s") 'sunrise)


;;;;;;;;;;;;;;;;;;;
;; MONOKAI THEME ;;
;;;;;;;;;;;;;;;;;;;
;;(load "~/.emacs.d/plugins/color-theme-molokai.el")
;;(color-theme-molokai)


;;;;;;;;;;;;;;;;;
;; numery lini ;;
;;;;;;;;;;;;;;;;;
(global-linum-mode 1)
(tool-bar-mode -1)


;;;;;;;;;;;;;;;;;
;; PYTHON-MODE ;;
;;;;;;;;;;;;;;;;;
(setq py-install-directory (concat esk-user-dir "/python-mode.el-6.0.12/"))
(add-to-list 'load-path py-install-directory)
(setq py-set-complete-keymap-p t)
(require 'python-mode)
(virtualenv-workon "~/.emacs.d/default/")


;;;;;;;;;;;;
;; PYMACS ;;
;;;;;;;;;;;;
(defun load-pycomplete ()
  "Load and initialize pycomplete."
  (interactive)
  (let* ((pyshell (py-choose-shell))
         (path (getenv "PYTHONPATH")))
    (setenv "PYTHONPATH" (concat
                          (expand-file-name py-install-directory) "completion"
                          (if path (concat path-separator path))))
    (if (py-install-directory-check)
        (progn
          (setenv "PYMACS_PYTHON" (if (string-match "IP" pyshell)
                                      "python" pyshell))
          (autoload 'pymacs-apply "pymacs")
          (autoload 'pymacs-call "pymacs")
          (autoload 'pymacs-eval "pymacs")
          (autoload 'pymacs-exec "pymacs")
          (autoload 'pymacs-load "pymacs")
          (load (concat py-install-directory "completion/pycomplete.el") nil t)
          (add-hook 'python-mode-hook 'py-complete-initialize))
      (error "`py-install-directory' not set, see INSTALL"))))
(eval-after-load 'pymacs '(load-pycomplete))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Trim Trailing Whitespace And Delete Blank Lines On Save ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'before-save-hook 'delete-trailing-whitespace
          'delete-blank-lines)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Delete Seleted Text When Typing ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(delete-selection-mode t)


;;;;;;;;;;;;;;;;;;;;;;
;; ADD SPECIAL NAME ;;
;;;;;;;;;;;;;;;;;;;;;;
(setq frame-title-format
          '("%f" (dired-directory +" - Go fishing my boy!")))


;;;;;;;;;;;;;;
;; FLYCHECK ;;
;;;;;;;;;;;;;;
(add-hook 'after-init-hook #'global-flycheck-mode)



(setq gc-cons-threshold 100000000)
(defconst spacemacs-version          "0.105.9" "Spacemacs version.")
(defconst spacemacs-emacs-min-version   "24.3" "Minimal version of Emacs.")

(if (not (version<= spacemacs-emacs-min-version emacs-version))
    (message (concat "Your version of Emacs (%s) is too old. "
                     "Spacemacs requires Emacs version %d or above.")
             emacs-version spacemacs-emacs-min-version)
  (load-file (concat user-emacs-directory "core/core-load-paths.el"))
  (require 'core-spacemacs)
  (spacemacs/init)
  (spacemacs/maybe-install-dotfile)
  (configuration-layer/sync)
  (spacemacs/setup-startup-hook)
  (require 'server)
  (unless (server-running-p) (server-start))
  (load-theme 'monokai t))

(provide 'init)
;;; init.el ends here
